import PropTypes from "prop-types";

import "./MovieItem.css";
import {useState} from "react";

function MovieItem(props) {
  const {title, released, director, poster} = props;

  // TODO create a "favorite" state, default value : false
  const [favorite, setFavorite] = useState(false);

  // TODO add an action when clicking on the favorite button
  function toggleFavorite() { /* Définition de la fonction qui change le state quand on click sur le bouton favorite plus bas */
    setFavorite(!favorite); /* setFavorite prends entre parenthèses ce qui doit etre change. Ici on prend l'inverse de l'état actuel de favorite. */
    console.log(favorite); /* Pour vérifier que le state ets bien changé à l'appel de la fonction. On peut le voir aussi avec l'extension react developer tools sur firefox dans le composant*/
  }

  return (
    <div className="MovieItem">
      <h2>{title}</h2>
      <img src={poster} alt={title} />
      <h4>Director: {director}</h4>
      <h5>Released: {released}</h5>
      <button type="button" onClick={toggleFavorite}>  {/* On change le state au clic */}
        {favorite ? "Remove from favorites" : "Add to favorites"} {/* On fait un opérateur ternaire */}
      </button>
      {/* Ancienne version trop longue :  */}
      {/* {!favorite && (
        <button type="button" onClick={toggleFavorite}>
          Add to favorites
        </button>
      )}
      {favorite && (
        <button type="button" onClick={toggleFavorite}>
          Remove from favorites
        </button>
      )} */}
    </div>
  );
}

MovieItem.propTypes = {
  title: PropTypes.string.isRequired,
  released: PropTypes.string.isRequired,
  director: PropTypes.string.isRequired,
  poster: PropTypes.string.isRequired,
};

export default MovieItem;
