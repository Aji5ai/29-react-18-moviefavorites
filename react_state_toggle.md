# React - State toggle

Apprend à basculer entre deux états.

## Ressources

- [https://gitlab.com/bastienapp/react18-moviefavorites](https://gitlab.com/bastienapp/react18-moviefavorites)
- [React Developer Tools](https://react.dev/learn/react-developer-tools)

## Contexte du projet

Ta nouvelle tâche à réaliser dans ce sprint est la suivante : tu vas reprendre une branche existante d'un site de cinéma, et modifier le code afin de pouvoir ajouter (ou retirer) un film aux favoris.

## Modalités pédagogiques

Il faut pouvoir basculer entre deux états en cliquant sur un même bouton :
- si le film n'est pas dans les favoris, il faut l'y ajouter
- si le film est déjà ajouté en favoris, il faut l'en retirer

Commence par faire un fork du dépôt suivant : [https://gitlab.com/bastienapp/react18-moviefavorites](https://gitlab.com/bastienapp/react18-moviefavorites).

Ensuite clone ton fork en local.

Modifie le contenu du composant `MovieItem` afin quel le clic sur le bouton appelle une fonction `toggleFavorite`, qui change l'état favorite à true s'il était à false, et à false s'il était à true.

Cette fois, pas de ressources pour t'aider, à toi de la trouver !

​Pour vérifier l'état du state `favorite`, tu peux utiliser l'extension [React Developer Tools](https://react.dev/learn/react-developer-tools) pour ton navigateur, qui te permet de naviguer dans les composant de ton application React, et de voir les valeurs des props et des states.

Ajoute des commentaires afin d'expliquer ton code.

Tu remarqueras qu'en rafraîchissant la page, l'état précédent du bouton d'ajout aux favoris est perdu : en effet, le state n'est pas prévu pour faire perdurer un état au rechargement d'une page. Il te faudrait utiliser le localStorage ou les cookies pour le sauvegarder en local, ou une base de données pour sauvegarder sur un serveur. Ce n'est pas demandé dans ce brief (sauf si tu veux te challenger).

## Modalités d'évaluation

- Un dépôt GitLab contient le code du projet
- Faire un fork du dépôt et cloner le fork en local
​- Ajouter un state "favorite", avec la valeur false par défaut
- Créé une fonction "toggleFavorite" et l'appeler au clic du bouton dans le JSX
- Faire en sorte que le texte du bouton devienne "Remove from favorites" si favorite est true, et "Add to favorites" si favorite est false

## Livrables

- Un lien vers GitLab

## Critères de performance

- Le code source est documenté
- Utiliser les normes de codage du langage
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions